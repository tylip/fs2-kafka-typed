package com.tylip.fs2kafka.typed

import cats.effect.Sync
import com.tylip.fs2kafka.typed.scalax.syntax._
import fs2.kafka.{Deserializer, Serializer}
import io.circe.jawn.JawnParser
import io.circe.{Decoder, Encoder, Json, Printer}

package object circe {

  private[this] val jawnParser = new JawnParser

  private def circeJsonDeserializer[F[_]](implicit
    F: Sync[F],
  ): Deserializer[F, Json] =
    Deserializer.lift(bytes => F.fromEither(jawnParser.parseByteArray(bytes)))

  implicit def circeDecoderDeserializer[F[_], T](implicit
    F: Sync[F],
    decoder: Decoder[T],
  ): Deserializer[F, T] =
    circeJsonDeserializer[F].flatMap(json =>
      Deserializer.lift(_ => F.fromEither(decoder.decodeJson(json))),
    )

  private def circeJsonSerializer[F[_]](implicit
    F: Sync[F],
  ): Serializer[F, Json] =
    Serializer.lift(json =>
      F.pure(Printer.noSpaces.printToByteBuffer(json).remainingArray),
    )

  implicit def circeEncoderSerializer[F[_], T](implicit
    F: Sync[F],
    encoder: Encoder[T],
  ): Serializer[F, T] =
    circeJsonSerializer.contramap(encoder.apply)

}
