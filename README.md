# fs2-kafka-typed

[fs2-kafka](https://fd4s.github.io/fs2-kafka/) extension
to define "typed" Kafka topics. Such typed topics incorporate a data type
accompanied by a serializer and deserializer allowing initializing consumers and
producers without repeating the topic's specified data type.

## Quick Start

To use within an [sbt](https://www.scala-sbt.org/) project, add the following
to your build.sbt file:

```
resolvers +=
  "tylip-public" at "https://nexus.tylip.com/repository/tylip-public/"

libraryDependencies +=
  "com.tylip" %% "fs2-kafka-typed-core" % version
```

Substituting the `version` with a preferred version of the library.
See [Releases](../../releases) for available versions.

## Base Usage

To create a [cats-effect](https://typelevel.org/cats-effect/) 
`Resource[F, KafkaTopic[F, K, V]]` for a topic for values of type `T` 
having keys of type `K` with an effect type `F` use


```
TopicParams(
  "topic-name",
  cleanupPolicy = CleanupPolicy.Compact,
).resource[F, K, T]
```

While initializing a topic resource checks if the specified topic exists and
creates it if needed using the settings provided in `TopicParams`.

Having a `topic: KafkaTopic[F, K, V]` a producer resource can be acquired with
`topic.producer` and a consumer resource can acquired with
`topic.consumer("consumer-group-id")`.

## Additional modules

### Circe SerDe Support

```
"com.tylip" %% "fs2-kafka-typed-circe" % version
```

Defines fs2-kafka `Serializer`s and `Deserializer`s for
[circe](https://circe.github.io/circe/)'s `Json` values as well as other types
supporting `Encoder`s and `Decoder`s.

Usage:

```
import com.tylip.fs2kafka.typed.circe._
```

## PureConfig Support

```
"com.tylip" %% "fs2-kafka-typed-pureconfig" % version
```

Defines `ConfigReader[KafkaConfig]` to read Kafka config from `*.conf` files
with [PureConfig](https://pureconfig.github.io/).

Usage:

```
import com.tylip.fs2kafka.typed.pureconfigmodule._
```
