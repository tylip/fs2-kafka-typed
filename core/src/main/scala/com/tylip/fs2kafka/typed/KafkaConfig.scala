package com.tylip.fs2kafka.typed

import cats.effect.{Concurrent, ContextShift, Resource, Sync}
import fs2.kafka._

import scala.language.higherKinds

final case class KafkaConfig(
  properties: Map[String, String],
  replicationFactor: Short,
) {

  private def adminClientSettings[F[_]: Sync] =
    AdminClientSettings[F]
      .withProperties(properties)

  def admin[F[_]: Concurrent: ContextShift]: Resource[F, KafkaAdminClient[F]] =
    KafkaAdminClient.resource(adminClientSettings[F])

  def consumerSettings[F[_], K, V](implicit
    F: Sync[F],
    keyDeserializer: Deserializer[F, K],
    valueDeserializer: Deserializer[F, V],
  ): ConsumerSettings[F, K, V] =
    ConsumerSettings[F, K, V](
      keyDeserializer = keyDeserializer,
      valueDeserializer = valueDeserializer,
    ).withAutoOffsetReset(AutoOffsetReset.Earliest)
      .withProperties(properties)

  def producerSettings[F[_], K, V](implicit
    F: Sync[F],
    keySerializer: Serializer[F, K],
    valueSerializer: Serializer[F, V],
  ): ProducerSettings[F, K, V] =
    ProducerSettings[F, K, V](
      keySerializer = keySerializer,
      valueSerializer = valueSerializer,
    ).withProperties(properties)
      .withRetries(1)
      // max.in.flight.requests.per.connection = 1 to preserve ordering
      // since retries are enabled
      .withMaxInFlightRequestsPerConnection(1)

}
