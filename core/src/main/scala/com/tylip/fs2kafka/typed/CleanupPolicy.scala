package com.tylip.fs2kafka.typed

import enumeratum.{Enum, EnumEntry}
import enumeratum.EnumEntry.Uncapitalised

import scala.collection.immutable

sealed trait CleanupPolicy extends EnumEntry with Uncapitalised

object CleanupPolicy extends Enum[CleanupPolicy] {

  val values: immutable.IndexedSeq[CleanupPolicy] = findValues

  case object Compact extends CleanupPolicy
  case object Delete extends CleanupPolicy

}
