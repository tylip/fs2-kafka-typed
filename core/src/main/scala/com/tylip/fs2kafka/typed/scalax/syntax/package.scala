package com.tylip.fs2kafka.typed.scalax

import java.nio.ByteBuffer

package object syntax {

  implicit class ByteBufferOps(val buffer: ByteBuffer) extends AnyVal {
    def remainingArray: Array[Byte] = {
      val b = new Array[Byte](buffer.remaining)
      buffer.get(b)
      b
    }
  }

}
