package com.tylip.fs2kafka.typed

import cats.Traverse
import cats.effect._
import cats.syntax.flatMap._
import cats.syntax.functor._
import cats.syntax.monadError._
import fs2.kafka._
import org.typelevel.log4cats.MessageLogger
import org.typelevel.log4cats.slf4j.Slf4jLogger
import org.apache.kafka.clients.admin.NewTopic

import scala.jdk.CollectionConverters._
import scala.language.higherKinds

final class TopicParams(
  val topicName: String,
  val cleanupPolicy: CleanupPolicy = CleanupPolicy.Delete,
)(implicit val kafkaConfig: KafkaConfig) {

  def newTopic: NewTopic =
    new NewTopic(topicName, 1, kafkaConfig.replicationFactor)
      .configs(
        Map("cleanup.policy" -> cleanupPolicy.entryName).asJava,
      )

  def init[F[_], K, V](implicit
    admin: KafkaAdminClient[F],
    F: Concurrent[F],
    cs: ContextShift[F],
    keySerializer: Serializer[F, K],
    valueSerializer: Serializer[F, V],
    keyDeserializer: Deserializer[F, K],
    valueDeserializer: Deserializer[F, V],
  ): F[KafkaTopic[F, K, V]] =
    KafkaTopic.init(this)

  def resource[F[_], K, V](implicit
    admin: KafkaAdminClient[F],
    F: Concurrent[F],
    cs: ContextShift[F],
    keySerializer: Serializer[F, K],
    valueSerializer: Serializer[F, V],
    keyDeserializer: Deserializer[F, K],
    valueDeserializer: Deserializer[F, V],
  ): Resource[F, KafkaTopic[F, K, V]] =
    Resource.eval(init[F, K, V])

  def unitKeyResource[F[_], V](implicit
    admin: KafkaAdminClient[F],
    F: Concurrent[F],
    cs: ContextShift[F],
    valueSerializer: Serializer[F, V],
    valueDeserializer: Deserializer[F, V],
  ): Resource[F, KafkaTopic[F, Unit, V]] =
    resource[F, Unit, V](
      implicitly,
      implicitly,
      implicitly,
      Serializer.unit,
      implicitly,
      Deserializer.unit,
      implicitly,
    )

  def stringKeyResource[F[_], V](implicit
    admin: KafkaAdminClient[F],
    F: Concurrent[F],
    cs: ContextShift[F],
    valueSerializer: Serializer[F, V],
    valueDeserializer: Deserializer[F, V],
  ): Resource[F, KafkaTopic[F, String, V]] =
    resource[F, String, V](
      implicitly,
      implicitly,
      implicitly,
      Serializer.string,
      implicitly,
      Deserializer.string,
      implicitly,
    )

}

object TopicParams {

  def apply(
    topicName: String,
    cleanupPolicy: CleanupPolicy = CleanupPolicy.Delete,
  )(implicit kafkaConfig: KafkaConfig) =
    new TopicParams(topicName, cleanupPolicy)

}

final class KafkaTopic[F[_], K, V] private (
  params: TopicParams,
  logger: MessageLogger[F],
)(implicit
  keyDeserializer: Deserializer[F, K],
  valueDeserializer: Deserializer[F, V],
  keySerializer: Serializer[F, K],
  valueSerializer: Serializer[F, V],
) {

  type D[X] = Deserializer[F, X]

  def init(implicit admin: KafkaAdminClient[F], F: Sync[F]): F[Unit] =
    for {
      _      <- logger.info("initializing")
      topics <- admin.listTopics.names
      _ <-
        if (topics.contains(params.topicName))
          logger.info("topic already exists")
        else
          for {
            _ <- admin.createTopic(params.newTopic)
            _ <- logger.info("topic created")
          } yield ()
    } yield ()

  def consumer(
    groupId: String,
    settings: ConsumerSettings[F, K, V] => ConsumerSettings[F, K, V] = identity,
  )(implicit
    concurrentEffect: ConcurrentEffect[F],
    contextShift: ContextShift[F],
    timer: Timer[F],
  ): Resource[F, KafkaConsumer[F, K, V]] =
    for {
      _ <- Resource.eval(logger.info(s"creating consumer in group $groupId"))
      result <-
        KafkaConsumer[F]
          .resource(
            settings(
              params.kafkaConfig.consumerSettings[F, K, V](
                concurrentEffect,
                catsSyntaxMonadError[D, Throwable, K](keyDeserializer)
                  .adaptError(enrichError(groupId)),
                catsSyntaxMonadError[D, Throwable, V](valueDeserializer)
                  .adaptError(enrichError(groupId)),
              ),
            ).withGroupId(groupId),
          )
          .evalTap(consumer =>
            for {
              _ <- consumer.subscribeTo(params.topicName)
              _ <- logger.info(s"created and subscribed consumer")
            } yield (),
          )
    } yield result

  private def enrichError(
    groupId: String,
  ): PartialFunction[Throwable, Throwable] = { case error =>
    ConsumerMessageDeserializationError(params.topicName, groupId, error)
  }

  def producer(implicit
    concurrentEffect: ConcurrentEffect[F],
    contextShift: ContextShift[F],
  ): Resource[F, TopicProducer[F, K, V]] =
    KafkaProducer
      .resource(params.kafkaConfig.producerSettings[F, K, V])
      .map(new TopicProducer(params.topicName, _))
      .evalTap(_ => logger.info(s"created producer"))

}

object KafkaTopic {

  def init[F[_], K, V](params: TopicParams)(implicit
    admin: KafkaAdminClient[F],
    F: Concurrent[F],
    cs: ContextShift[F],
    keySerializer: Serializer[F, K],
    valueSerializer: Serializer[F, V],
    keyDeserializer: Deserializer[F, K],
    valueDeserializer: Deserializer[F, V],
  ): F[KafkaTopic[F, K, V]] =
    for {
      logger <- Slf4jLogger.fromName(s"kafka.topics.${params.topicName}")
      topic = new KafkaTopic[F, K, V](params, logger)
      _ <- topic.init
    } yield topic

}

final class TopicProducer[F[_], K, V](
  topicName: String,
  producer: KafkaProducer[F, K, V],
) {

  def produce(key: K, value: V): F[F[ProducerResult[K, V, Unit]]] =
    producer.produce(
      ProducerRecords.one(
        ProducerRecord(topicName, key, value),
      ),
    )

  def produce(record: ConsumerRecord[K, V]): F[F[ProducerResult[K, V, Unit]]] =
    produce(record.key, record.value)

  def produce[C[+_]](
    records: C[(K, V)],
  )(implicit F: Traverse[C]): F[F[ProducerResult[K, V, Unit]]] =
    producer.produce(
      ProducerRecords(
        records.map { case (key, value) =>
          ProducerRecord(topicName, key, value)
        },
      ),
    )

}

case class ConsumerMessageDeserializationError(
  topicName: String,
  groupId: String,
  override val getCause: Throwable,
) extends Exception {
  override def getMessage: String =
    s"A consumer in group $groupId failed " +
      s"to deserialize a topic $topicName message"
}
