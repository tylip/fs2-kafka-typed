package com.tylip.fs2kafka.typed

import pureconfig.generic.ProductHint
import pureconfig.{CamelCase, ConfigFieldMapping, ConfigReader}
import pureconfig.generic.semiauto._

package object pureconfigmodule {

  implicit private def productHint[T]: ProductHint[T] =
    ProductHint(fieldMapping = ConfigFieldMapping(CamelCase, CamelCase))

  implicit val kafkaConfigReader: ConfigReader[KafkaConfig] =
    deriveReader[KafkaConfig]

}
